﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float speed = 1; //public é necessária para disponibilizar a variável no Inspector
    public float jumpForce = 1;
    [Space]
    public Vector2 groundcheckStart;
    public Vector2 groundcheckEnd;
    Rigidbody2D rb2D; //RigidBody possui acesso a toda fisica do objeto, usaremos para controlar a movimentação e o pulo

	// Use this for initialization
	void Start ()
    {
        // Ao iniciar a cena, o controlador de movimento vai buscar a referencia do Rigidbody
        rb2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Move();
        Jump();
	}
    
    private void Move()
    {
        float axisX = Input.GetAxis("Horizontal") * speed * Time.deltaTime; //Verificar o valor do Axis Horizontal
        float posX = rb2D.position.x + axisX; //Atualiza o valor da posição X
        float posY = rb2D.position.y; //Posição y se mantem a mesma
        rb2D.position = new Vector2(posX, posY); //Atualiza com os novos valores

        //Nessa parte invertemos os sprites para virar para o lado em que estão andando.
        if (axisX > 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (axisX < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb2D.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        }
        
    }

    public bool IsGrounded()
    {
        Vector2 lineBegin = new Vector2(transform.position.x + groundcheckStart.x, transform.position.y + groundcheckStart.y);
        Vector2 lineEnd = new Vector2(transform.position.x + groundcheckEnd.x, transform.position.y + groundcheckEnd.y);
        bool groundCheck = Physics2D.Linecast(lineBegin, lineEnd);
        Debug.DrawLine(lineBegin, lineEnd);
        return groundCheck;
    }
}
