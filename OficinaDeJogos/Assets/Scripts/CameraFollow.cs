﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public Vector2 cameraOffset;
    public float delay;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float futurePosX = player.position.x + cameraOffset.x;
        float futurePosY = player.position.y + cameraOffset.y;

        transform.position = (Vector3.Lerp(transform.position, new Vector3(futurePosX, futurePosY, transform.position.z), delay));
	}
}
