﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPlayableCharacter : MonoBehaviour
{
    public GameObject player;
    SpriteRenderer rend;

    string currentText;
	// Use this for initialization
	void Start ()
    {
        rend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (player.transform.position.x < transform.position.x)
        {
            rend.flipX = true;
        }
        else
        {
            rend.flipX = false;
        }
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == player)
        {
            if (Input.GetButtonDown("Submit"))
            {
                CheckMission(player);
                Debug.Log(currentText);
            }
            
        }
    }

    private void CheckMission(GameObject player)
    {
        ItemCollector collector = player.GetComponent<ItemCollector>();
        if (collector.itensCollected == 3)
        {
            currentText = "Obrigado pela ajuda! Agora podemos fazer sua requisição finalmente!";
        }
        else if (collector.itensCollected == 0)
        {
            currentText = "Olá, você quer fazer a matricula? Então preciso que me ajude a procurar 3 itens para isso! Minha caneta, o carimbo e o formulário de requisição";
        }
        else if (collector.itensCollected < 3)
        {
            currentText = "Ainda faltam " + (3 - collector.itensCollected).ToString() + " itens para fazer a matricula!";
        }
    }
}
