﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator anim;
    MovementController mov;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        mov = GetComponent<MovementController>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetAxis("Horizontal") != 0 && mov.IsGrounded())
        {
            anim.SetTrigger("Walk");
            anim.ResetTrigger("Jump");
            anim.ResetTrigger("Stop");

        }
        else if (Input.GetAxis("Horizontal") == 0 && mov.IsGrounded())
        {
            anim.SetTrigger("Stop");
            anim.ResetTrigger("Walk");
            anim.ResetTrigger("Jump");

        }
        if (Input.GetButtonDown("Jump") && mov.IsGrounded())
        {
            anim.SetTrigger("Jump");
            anim.ResetTrigger("Walk");
            anim.ResetTrigger("Stop");

        }

    }
}
