﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollector : MonoBehaviour
{
    public int itensCollected { get; set; }

	// Use this for initialization
	void Start ()
    {
        itensCollected = 0;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Item")
        {
            itensCollected++;
            Destroy(collision.gameObject);
        }
    }
}
